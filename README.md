Клиент-серверное приложение общающееся через grpc.

Для запуска требуется python>=3.8

Для запуска сервера: python3.8 server.py

Для запуска клиента:
 
python3.8 client.py pogr.7z password.txt 2

python3.8 client.py pogr.7z password2.txt 2
                     
password*.txt - Файлы с паролями. 

pogr.7z - запароленный архив. Пароль - LONGSTRONGPASSWORD
