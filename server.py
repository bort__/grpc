import asyncio
from time import sleep
from random import random
import aiofiles as aiofiles
import grpc
from concurrent import futures

import py7zr as py7zr

import unary_pb2_grpc as pb2_grpc
import unary_pb2 as pb2



async def open_zip(archive, filename):
    async with aiofiles.open(filename, "r") as file:
        async for password in file:
            p = password.rstrip()
            with py7zr.SevenZipFile(archive, mode='r', password=p) as z:
                try:
                    z.extract()
                    print(f"{p} is correct")
                    return p
                except Exception as e:
                    print(f"{filename} | {p} is incorrect")



class UnaryService(pb2_grpc.UnaryServicer):
    async def CheckPassword(self, request, context):
        queue = asyncio.PriorityQueue()  # это нифига не очередь с приоритетом
        await queue.put((request.priority, await open_zip(request.archive_path, request.password)))
        _, password = await queue.get()
        if password:
            return pb2.Response(correct=True, password=password)
        else:
            return pb2.Response(correct=False, password="")


async def serve(host, port):
    server = grpc.aio.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_UnaryServicer_to_server(UnaryService(), server)
    server.add_insecure_port(f"{host}:{port}")
    await server.start()
    print("STARTED")
    await server.wait_for_termination()


if __name__ == '__main__':
    asyncio.run(serve("127.0.0.1", 12735))
