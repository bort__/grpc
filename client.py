import sys
import grpc
import unary_pb2_grpc as pb2_grpc
import unary_pb2 as pb2


class UnaryClient(object):
    """
    Client for gRPC functionality
    """

    def __init__(self, archive, file, priority, host="127.0.0.1", port=12735):
        self.archive = archive
        self.file = file
        self.priority = int(priority)
        self.host = host
        self.port = port
        channel = grpc.insecure_channel(f"{self.host}:{self.port}")
        self.stub = pb2_grpc.UnaryStub(channel)

    def send_message(self):
        message = pb2.Message(archive_path=self.archive, password=self.file, priority=self.priority)
        print(f"Message:\n{message}")
        response = self.stub.CheckPassword(message)
        print(f"Response:\n{response}")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Must be at least 3 arguments: path to archive? path to file with password and priority")
        exit()

    archive = sys.argv[1]
    file = sys.argv[2]
    priority = sys.argv[3]

    client = UnaryClient(archive, file, priority)
    client.send_message()
